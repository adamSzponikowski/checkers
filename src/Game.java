import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private Board board = new Board();
    private Scanner scanner = new Scanner(System.in);

    private Player p1 = new Player();
    private Player p2 = new Player();
    private Player AI = new Player();

    private void choosingTheMods() {
        System.out.println("Welcome in the Checkers Game");
        System.out.println("Choose the game mode: ");
        System.out.println("1.Player vs Player");
        System.out.println("2.Player vs Computer");
    }

    public void welcome() {
        choosingTheMods();
        int playerChoice = scanner.nextInt();
        if (playerChoice == 1) {
            playerVsPlayer();
            board.initializeBoard();
            moves();
        }
        if (playerChoice == 2) {
            playerVsAi();
            board.initializeBoard();
            movesVsAi();
        }


    }

    public boolean gameOver() {
        if (p1.playerCheckers == 0 && p2.playerCheckers > 0) {
            System.out.println("Game is over," + p2.getName() + "have won");
            return true;
        }
        if (p1.playerCheckers > 0 && p2.playerCheckers == 0) {
            System.out.println("Game is over" + p1.getName() + "have won");
            return true;
        }
        return false;
    }

    public boolean gameOverVsAi() {
        if (p1.playerCheckers == 0 && AI.playerCheckers > 0) {
            System.out.println("Game is over," + AI.getName() + "have won");
            return true;
        }
        if (p1.playerCheckers > 0 && AI.playerCheckers == 0) {
            System.out.println("Game is over" + p1.getName() + "have won");
            return true;
        }
        return false;
    }

    public void settingPlayer1() {
        System.out.println("Player 1, you are going to play white, as 1");
        p1.createPlayerName();
        p1.setCheckerType("1");
        p1.setYourTurn(true);
        p1.setDirection("Up");
    }

    public void playerVsPlayer() {
        settingPlayer1();
        System.out.println("Player 2, you are going to play black, as A");
        p2.createPlayerName();
        p2.setCheckerType("A");
        p2.setDirection("Down");
    }

    public void playerVsAi() {
        settingPlayer1();
        System.out.println("Player 2, is computer named Bob");
        AI.setName("Bob");
        AI.setCheckerType("A");
        AI.setDirection("Down");
    }

    public void movesVsAi() {
        while (!gameOverVsAi()) {
            Player currentplayer = p1.isYourTurn() ? p1 : AI;
            if (p1.isYourTurn()) {
                Scanner sc = new Scanner(System.in);
                System.out.println(currentplayer.getName() + " Turn");
                System.out.println("Enter piece you want to move and where f.e [1,1][2,2]");
                String playerCoordinates = sc.nextLine();
                PlayerChoice playerChoice = parsePlayerChoice(playerCoordinates);
                if (isBeatPossible(currentplayer)) {
                    if (didPlayerTakeTheBeat(currentplayer, playerChoice)) {
                        moveForward(playerChoice.checkerCoordinates(), playerChoice.moveToCoordinates(), currentplayer);
                    } else {
                        System.out.println("You have to take the beat!");
                        movesVsAi();
                    }
                }
                moveForward(playerChoice.checkerCoordinates(), playerChoice.moveToCoordinates(), currentplayer);
            } else {
                PlayerChoice playerChoice = aiRandomChoice(currentplayer);
                if (isBeatPossible(currentplayer)) {
                    if (didPlayerTakeTheBeat(currentplayer, playerChoice)) {
                        moveForward(playerChoice.checkerCoordinates(), playerChoice.moveToCoordinates(), currentplayer);
                    } else {
                        movesVsAi();
                    }
                } else if (!isAiMoveValid(playerChoice.checkerCoordinates(), playerChoice.moveToCoordinates(), currentplayer)) {
                    movesVsAi();
                }
                moveForward(playerChoice.checkerCoordinates(), playerChoice.moveToCoordinates(), currentplayer);

                board.currentBoard();

            }
            if (p1.isYourTurn()) {
                p1.setYourTurn(false);
                AI.setYourTurn(true);
            } else {
                p1.setYourTurn(true);
                AI.setYourTurn(false);
            }


        }
    }


    public void moves() {
        Scanner sc = new Scanner(System.in);
        while (!gameOver()) {
            Player currentplayer = p1.isYourTurn() ? p1 : p2;
            System.out.println(currentplayer.getName() + " Turn");
            System.out.println("Enter piece you want to move and where f.e [1,1][2,2]");
            String playerCoordinates = sc.nextLine();
            PlayerChoice playerChoice = parsePlayerChoice(playerCoordinates);
            getBeatCoordinates(currentplayer);
            if (isBeatPossible(currentplayer)) {
                if (didPlayerTakeTheBeat(currentplayer, playerChoice)) {
                    moveForward(playerChoice.checkerCoordinates(), playerChoice.moveToCoordinates(), currentplayer);
                } else {
                    System.out.println("You have to take the beat!");
                    moves();
                }
            }
            moveForward(playerChoice.checkerCoordinates(), playerChoice.moveToCoordinates(), currentplayer);
            board.currentBoard();
            if (p1.isYourTurn()) {
                p1.setYourTurn(false);
                p2.setYourTurn(true);
            } else {
                p1.setYourTurn(true);
                p2.setYourTurn(false);
            }


        }
    }


    private PlayerChoice parsePlayerChoice(String rawChoice) {
        String rawCheckerPosition = rawChoice.substring(1, 4);
        String rawMovePosition = rawChoice.substring(6, 9);
        int checkPositionX = Integer.parseInt(rawCheckerPosition.substring(0, 1));
        int checkPositionY = Integer.parseInt(rawCheckerPosition.substring(2, 3));
        int moveToPositionX = Integer.parseInt(rawMovePosition.substring(0, 1));
        int moveToPositionY = Integer.parseInt(rawMovePosition.substring(2, 3));
        return new PlayerChoice(
                new Coordinates(checkPositionX, checkPositionY),
                new Coordinates(moveToPositionX, moveToPositionY)
        );
    }

    public void moveForward(Coordinates checkerCoordinates, Coordinates moveToCoordinates, Player player) {
        isMoveValid(checkerCoordinates, moveToCoordinates, player);
    }

    public void isMoveValid(Coordinates checkerCoordinates, Coordinates moveToCoordinates, Player player) {
        String beatPosition = board.gameboard[(checkerCoordinates.y() + moveToCoordinates.y()) / 2][(checkerCoordinates.x() + moveToCoordinates.x()) / 2];
        String direction = player.getDirection();
        String checkerPosition = board.gameboard[checkerCoordinates.y()][checkerCoordinates.x()];
        String movePosition = board.gameboard[moveToCoordinates.y()][moveToCoordinates.x()];
        if (checkerPosition.equals(player.getCheckerType())) {
            if (isPositionEmpty(movePosition)) {
                if (isXMoveValid(checkerCoordinates, moveToCoordinates)) {
                    if (isYMoveValid(checkerCoordinates, moveToCoordinates, direction)) {
                        board.gameboard[checkerCoordinates.y()][checkerCoordinates.x()] = "-";
                        board.gameboard[moveToCoordinates.y()][moveToCoordinates.x()] = player.getCheckerType();
                    }
                }
                if (isMoveABeat(beatPosition, player)) {
                    int beatX = (checkerCoordinates.x() + moveToCoordinates.x()) / 2;
                    int beatY = (checkerCoordinates.y() + moveToCoordinates.y()) / 2;
                    if (p1.isYourTurn()) {
                        p2.playerCheckers = p2.playerCheckers - 1;
                    } else {
                        p1.playerCheckers = p1.playerCheckers - 1;
                    }

                    board.gameboard[beatY][beatX] = "-";
                    board.gameboard[checkerCoordinates.y()][checkerCoordinates.x()] = "-";
                    board.gameboard[moveToCoordinates.y()][moveToCoordinates.x()] = player.getCheckerType();
                }
            }
        }
    }


    private boolean isPositionEmpty(String movePosition) {
        return movePosition.equals("-");
    }

    private boolean isMoveABeat(String beatPosition, Player player) {
        return !isPositionEmpty(beatPosition) && !beatPosition.equals(player.getCheckerType());
    }


    private boolean isYMoveValid(Coordinates checkerCoordinates, Coordinates moveToCoordinates, String direction) {
        return isYForUpValid(checkerCoordinates, moveToCoordinates, direction) ||
                isYForDownValid(checkerCoordinates, moveToCoordinates, direction);
    }

    private boolean isYForUpValid(Coordinates checkerCoordinates, Coordinates moveToCoordinates, String direction) {
        return direction.equals("Up") && moveToCoordinates.y() == checkerCoordinates.y() - 1;
    }

    private boolean isYForDownValid(Coordinates checkerCoordinates, Coordinates moveToCoordinates, String direction) {
        return direction.equals("Down") && moveToCoordinates.y() == checkerCoordinates.y() + 1;
    }

    private boolean isXMoveValid(Coordinates checkerCoordinates, Coordinates moveToCoordinates) {
        return moveToCoordinates.x() == checkerCoordinates.x() + 1 || moveToCoordinates.x() == checkerCoordinates.x() - 1;
    }

    private ArrayList<PlayerChoice> getBeatCoordinates(Player player) {
        ArrayList<PlayerChoice> beatCoordinates = new ArrayList<>();
        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                if (board.gameboard[y][x].equals(player.getCheckerType())) {
                    for (int i = -1; i <= 1; i += 2) {
                        for (int j = -1; j <= 1; j += 2) {
                            int newX = y + i;
                            int newY = x + j;
                            if (newX >= 0 && newX < 8 && newY >= 0 && newY < 8
                                    && !board.gameboard[newX][newY].equals(player.getCheckerType()) && !board.gameboard[newX][newY].equals("-")) {
                                int nextX = newX + i;
                                int nextY = newY + j;
                                if (nextX >= 0 && nextX < 8 && nextY >= 0 && nextY < 8
                                        && board.gameboard[nextX][nextY].equals("-")) {
                                    Coordinates checkerCoordinates = new Coordinates(x, y);
                                    Coordinates possibleBeatMoves = new Coordinates(nextY, nextX);
                                    beatCoordinates.add(new PlayerChoice(checkerCoordinates, possibleBeatMoves));
                                }
                            }
                        }
                    }
                }
            }
        }
        return beatCoordinates;

    }

    private boolean isBeatPossible(Player player) {
        ArrayList<PlayerChoice> beatCoordinates = getBeatCoordinates(player);
        if (beatCoordinates.isEmpty()) {
            return false;
        }
        return true;
    }

    private boolean didPlayerTakeTheBeat(Player player, PlayerChoice playerChoice) {
        ArrayList<PlayerChoice> possibleBeatMoves = getBeatCoordinates(player);
        if (isBeatPossible(player)) {
            return possibleBeatMoves.contains(playerChoice);
        }
        return false;
    }

    private PlayerChoice aiRandomChoice(Player player) {
        ArrayList<PlayerChoice> beatCoordinates = getBeatCoordinates(player);
        if (!beatCoordinates.isEmpty()) {
            return beatCoordinates.get(0);
        }
        Random random = new Random();
        int checkPositionX = random.nextInt(8);
        int checkPositionY = random.nextInt(8);
        int moveToX = random.nextInt(8);
        int moveToY = random.nextInt(8);
        return new PlayerChoice(
                new Coordinates(checkPositionX, checkPositionY),
                new Coordinates(moveToX, moveToY)
        );
    }

    private boolean isAiMoveValid(Coordinates checkerCoordinates, Coordinates moveToCoordinates, Player player) {
        String direction = player.getDirection();
        String checkerPosition = board.gameboard[checkerCoordinates.y()][checkerCoordinates.x()];
        String movePosition = board.gameboard[moveToCoordinates.y()][moveToCoordinates.x()];
        if (checkerPosition.equals(player.getCheckerType())) {
            if (isPositionEmpty(movePosition)) {
                if (isXMoveValid(checkerCoordinates, moveToCoordinates)) {
                    return isYMoveValid(checkerCoordinates, moveToCoordinates, direction);
                }

            }
        }
        return false;
    }


}












