
public class Board {

    String[][] gameboard = new String[8][8];

    public void initializeBoard() {
        boolean isDark;
        System.out.print("  0 1 2 3 4 5 6 7");
        System.out.println();
        for (int row = 0; row < 8; row++) {
            System.out.print(row + " ");
            for (int column = 0; column < 8; column++) {
                isDark = (row + column) % 2 == 1;
                if (row < 3 && isDark) {
                    gameboard[row][column] = "A";
                    System.out.print(gameboard[row][column] + " ");
                }
                if (row > 4 && isDark) {
                    gameboard[row][column] = "1";
                    System.out.print(gameboard[row][column] + " ");
                }
                if (!isDark || row == 3 || row == 4) {
                    gameboard[row][column] = "-";
                    System.out.print(gameboard[row][column] + " ");
                }
            }
            System.out.println();
        }
    }

    public void currentBoard() {
        boolean isDark;
        System.out.print("  0 1 2 3 4 5 6 7");
        System.out.println();
        for (int row = 0; row < 8; row++) {
            System.out.print(row + " ");
            for (int column = 0; column < 8; column++) {
                isDark = (row + column) % 2 == 1;
                if (row < 3 && isDark) {
                    System.out.print(gameboard[row][column] + " ");
                }
                if (row > 4 && isDark) {
                    System.out.print(gameboard[row][column] + " ");
                }
                if (!isDark || row == 3 || row == 4) {
                    System.out.print(gameboard[row][column] + " ");
                }

            }
            System.out.println();
        }
    }


}



