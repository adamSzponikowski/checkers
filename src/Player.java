import java.util.Scanner;

public class Player {
    public String name;
    private Scanner sc = new Scanner(System.in);
    public int playerCheckers = 12;

    private String direction;

    private boolean yourTurn = true;

    public String checkerType;

    public void setCheckerType(String checkerType) {
        this.checkerType = checkerType;
    }

    public String getCheckerType() {
        return checkerType;
    }

    public void setDirection(String direction){
        this.direction = direction;
    }

    public String getDirection(){
        return direction;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setYourTurn(boolean yourTurn){
        this.yourTurn = yourTurn;
    }

    public boolean isYourTurn() {
        return yourTurn;
    }

    public void nameIsEmpty() {
        getName();
        while (name.isEmpty()) {
            System.out.println("Your name is empty, you have to provide it!");
            createPlayerName();
        }
    }

    public void createPlayerName() {
        System.out.println("Please provide your name");
        String name = sc.nextLine();
        setName(name);
        nameIsEmpty();
    }

}

